# Django car app

## Make rules

### `make up`
Builds the docker container and starts the project.

### `make makemigrations`
Creates migrations and apply them.

### `make test`
Runs Pytest.

### `make black`
Runs Black to format all python files.

## Environment variables
Look at `example.env`
