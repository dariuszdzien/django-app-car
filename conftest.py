import pytest
from pytest_factoryboy import register

from content.tests.factories import CarFactory, RateFactory

register(CarFactory)
register(RateFactory)


@pytest.fixture
def api_client():
    from rest_framework.test import APIClient

    return APIClient()
