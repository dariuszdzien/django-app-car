up:
	docker-compose up --build

makemigrations:
	docker-compose run --rm -T web sh -c "./manage.py makemigrations"
	docker-compose run --rm -T web sh -c "./manage.py migrate"

black:
	docker-compose run --rm -T web sh -c "black ."

test:
	docker-compose run --rm -T web sh -c "pytest"