from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Avg


class Car(models.Model):
    maker = models.CharField(max_length=256)
    model = models.CharField(max_length=256)

    class Meta:
        unique_together = (
            "maker",
            "model",
        )

    def __str__(self):
        return f"{self.maker} - {self.model}"

    def rate(self):
        avg_rate = self.rates.all().aggregate(Avg("rate")).get("rate__avg")
        return round(avg_rate, 2) if avg_rate else None


class Rate(models.Model):
    rate = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    car = models.ForeignKey(Car, on_delete=models.CASCADE, related_name="rates")

    def __str__(self):
        return f"{self.car} - {self.rate}"
