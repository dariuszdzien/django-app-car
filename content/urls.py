from django.urls import path

from .views import CarLiesView, PopularCarLiesView, RateCarView

urlpatterns = [
    path("cars/", CarLiesView.as_view(), name="cars"),
    path("popular/", PopularCarLiesView.as_view(), name="popular-cars"),
    path("rate/", RateCarView.as_view(), name="rate-car"),
]
