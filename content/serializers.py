from rest_framework import serializers

from .models import Car, Rate
from .services.vehicle_api import get_models_for_maker


class RateCarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rate
        fields = ["rate", "car"]


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ["maker", "model", "rate"]

    def validate(self, data):
        data_maker = data["maker"].lower()
        data_model = data["model"].lower()
        results = get_models_for_maker(data_maker)
        model = False
        maker = False
        for result in results:
            if result["Make_Name"].lower() == data_maker:
                maker = True
            if result["Model_Name"].lower() == data_model:
                model = True
            if (
                result["Make_Name"].lower() == data_maker
                and result["Model_Name"].lower() == data_model
            ):
                if Car.objects.filter(
                    model=result["Model_Name"], maker=result["Make_Name"]
                ):
                    raise serializers.ValidationError(
                        f"{data_maker} - {data_model} already exist in database."
                    )
                return {"maker": result["Make_Name"], "model": result["Model_Name"]}
        error = {}
        if not maker:
            error["maker"] = f"Maker {data_maker} does't exist."
        if not model:
            error["model"] = f"Model {data_model} does't exist."
        raise serializers.ValidationError(error)
