import requests


def get_models_for_maker(maker: str):
    maker = maker.lower()
    r = requests.get(
        f"https://vpic.nhtsa.dot.gov/api/vehicles/GetModelsForMake/{maker}?format=json"
    )
    results = r.json().get("Results")
    return results
