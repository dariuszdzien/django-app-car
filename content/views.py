from django.db.models import Count
from rest_framework import generics

from .models import Car, Rate
from .serializers import CarSerializer, RateCarSerializer


class CarLiesView(generics.ListCreateAPIView):
    queryset = Car.objects.all().prefetch_related("rates")
    serializer_class = CarSerializer


class PopularCarLiesView(generics.ListAPIView):
    queryset = (
        Car.objects.annotate(rates_count=Count("rates"))
        .prefetch_related("rates")
        .order_by("-rates_count")
    )
    serializer_class = CarSerializer


class RateCarView(generics.CreateAPIView):
    queryset = Rate.objects.all()
    serializer_class = RateCarSerializer
