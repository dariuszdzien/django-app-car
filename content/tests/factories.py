import factory
from factory import fuzzy

from content.models import Car, Rate


class CarFactory(factory.django.DjangoModelFactory):
    maker = fuzzy.FuzzyText(length=25)
    model = fuzzy.FuzzyText(length=25)

    class Meta:
        model = Car


class RateFactory(factory.django.DjangoModelFactory):
    rate = fuzzy.FuzzyInteger(1, 5)
    car = factory.SubFactory(CarFactory)

    class Meta:
        model = Rate
