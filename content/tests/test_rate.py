import pytest
from django.urls import reverse
from rest_framework import status

pytestmark = pytest.mark.django_db


def test_rate_car_wrong_max_rate(api_client, car):
    url = reverse("rate-car")
    response = api_client.post(url, data={"rate": 6, "car": car.id}, type="json")
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        "rate": ["Ensure this value is less than or equal to 5."]
    }

    response = api_client.post(url, data={"rate": 0, "car": car.id}, type="json")
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        "rate": ["Ensure this value is greater than or equal to 1."]
    }


def test_rate_car_wrong_min_rate(api_client):
    url = reverse("rate-car")
    response = api_client.post(url, data={"rate": 1, "car": 1}, type="json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {"car": ['Invalid pk "1" - object does not exist.']}


def test_rate_car(api_client, car):
    url = reverse("rate-car")
    response = api_client.post(url, data={"rate": 1, "car": car.id}, type="json")

    assert response.status_code == status.HTTP_201_CREATED
    assert response.json() == {"rate": 1, "car": car.id}
    assert car.rates.count() == 1
    assert car.rates.first().rate == 1
