import pytest
from django.urls import reverse
from rest_framework import status

pytestmark = pytest.mark.django_db


def test_car_rate(car, rate_factory):
    rate_factory(car=car, rate=1)
    rate_factory(car=car, rate=2)
    rate_factory(car=car, rate=4)

    assert car.rate() == 2.33


def test_car_list_empty(api_client):
    url = reverse("cars")
    response = api_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert response.json() == []


def test_car_list(api_client, car, rate_factory):
    url = reverse("cars")
    rate_factory(car=car, rate=1)
    rate_factory(car=car, rate=2)
    rate_factory(car=car, rate=3)

    response = api_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert response.json() == [
        {
            "maker": car.maker,
            "model": car.model,
            "rate": 2,
        }
    ]


def test_post_care(api_client):
    url = reverse("cars")
    response = api_client.post(
        url, data={"maker": "honda", "model": "accord"}, type="json"
    )
    assert response.status_code == status.HTTP_201_CREATED
    assert response.json() == {"maker": "HONDA", "model": "Accord", "rate": None}


def test_post_care_already_exist(api_client, car_factory):
    url = reverse("cars")
    car_factory(model="Accord", maker="HONDA")
    response = api_client.post(
        url, data={"maker": "honda", "model": "accord"}, type="json"
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        "non_field_errors": ["honda - accord already exist in database."]
    }


def test_post_care_does_not_exist(api_client):
    url = reverse("cars")
    response = api_client.post(
        url, data={"maker": "XXX XXX", "model": "XXX"}, type="json"
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        "model": ["Model xxx does't exist."],
        "maker": ["Maker xxx xxx does't exist."],
    }


def test_car_popular_list_empty(api_client):
    url = reverse("popular-cars")
    response = api_client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == []


def test_car_popular_list(api_client, car_factory, rate_factory):
    url = reverse("popular-cars")
    car_one = car_factory()
    car_two = car_factory()
    car_three = car_factory()
    rate_factory(car=car_one, rate=1)
    rate_factory(car=car_one, rate=1)
    rate_factory(car=car_two, rate=3)

    response = api_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 3
    assert response.json()[0] == {
        "maker": car_one.maker,
        "model": car_one.model,
        "rate": 1,
    }
    assert response.json()[1] == {
        "maker": car_two.maker,
        "model": car_two.model,
        "rate": 3,
    }
    assert response.json()[2] == {
        "maker": car_three.maker,
        "model": car_three.model,
        "rate": None,
    }
